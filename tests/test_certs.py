"""Test cki_lib.certs module."""
from datetime import timedelta
import pathlib
import tempfile
import unittest
from unittest import mock

from cki_lib import misc

try:
    from cryptography import x509
    from cryptography.hazmat import primitives

    from cki_lib import certs

    NO_CRYPTOGRAPHY = False
except ImportError:
    NO_CRYPTOGRAPHY = True


@unittest.skipIf(NO_CRYPTOGRAPHY, 'cryptography is not installed')
class TestMetrics(unittest.TestCase):
    """Test cert metrics."""

    def test_expiry(self) -> None:
        """Test certificate expiry."""
        expiry = misc.now_tz_utc().replace(microsecond=0) + timedelta(days=10)
        with tempfile.NamedTemporaryFile() as temp:
            key = primitives.asymmetric.rsa.generate_private_key(65537, 2048)
            cert = (
                x509.CertificateBuilder()
                    .subject_name(x509.Name([]))
                    .issuer_name(x509.Name([]))
                    .public_key(key.public_key())
                    .serial_number(x509.random_serial_number())
                    .not_valid_before(misc.now_tz_utc())
                    .not_valid_after(expiry)
                    .sign(key, primitives.hashes.SHA256())
            )
            pathlib.Path(temp.name).write_bytes(
                cert.public_bytes(primitives.serialization.Encoding.PEM)
            )

            with (mock.patch.dict('os.environ', {'CKI_METRICS_ENABLED': 'true'}),
                  mock.patch('cki_lib.certs.METRIC_CERTIFICATE_NOT_VALID_AFTER') as mock_metric):
                certs.update_certificate_metrics(temp.name)
                mock_metric.assert_has_calls([
                    mock.call.labels(temp.name),
                    mock.call.labels().set(expiry.timestamp()),
                ])

            with (mock.patch.dict('os.environ', {'CKI_METRICS_ENABLED': 'false'}),
                  mock.patch('cki_lib.certs.METRIC_CERTIFICATE_NOT_VALID_AFTER') as mock_metric):
                certs.update_certificate_metrics(temp.name)
                mock_metric.assert_not_called()
