"""Kubernetes-based integration tests."""

from . import cluster
from . import minio  # deprecated import
from . import vault  # deprecated import
from .cluster import skip_without_requirements  # deprecated import

__all__ = [
    "KubernetesIntegrationTest",  # deprecated, do not use
    "cluster",  # deprecated, import cki_lib.inttests.cluster
    "minio",  # deprecated, import cki_lib.inttests.minio
    "skip_without_requirements",  # deprecated, use cluster.skip_without_requirements
    "vault",  # deprecated, import cki_lib.inttests.vault
]


class KubernetesIntegrationTest(cluster.KubernetesCluster):
    # deprecated, do not use
    """Kubernetes-based integration test."""

    namespace = 'integration-test'

    def setUp(self) -> None:
        """Provision the namespace."""
        super().setUp()
        self.enterContext(self.k8s_namespace(self.namespace))
