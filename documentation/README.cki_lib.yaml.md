---
title: cki_lib.yaml
linkTitle: yaml
description: Work with YAML files
---

## Set keys in YAML data

```bash
usage: python3 -m cki_lib.yaml set-value [-h] [--format {yaml,json}] key value

Set a dot-delimited key

positional arguments:
  key         dot-delimited key
  value       YAML-formatted value

options:
  -h, --help  show this help message and exit
  --format {yaml,json}  output format (default: yaml)
```

Reads from stdin and writes to stdout.

Example:

```console
$ python -m cki_lib.yaml set-value foo.1 'qux: true' <<< 'foo: [bar, baz]'
---
foo:
  - bar
  - qux: true
```

## Delete keys from YAML data

```bash
usage: python3 -m cki_lib.yaml del [-h] [--format {yaml,json}] key

Set a dot-delimited key

positional arguments:
  key         dot-delimited key

options:
  -h, --help  show this help message and exit
  --format {yaml,json}  output format (default: yaml)
```

Reads from stdin and writes to stdout.

Example:

```console
$ python -m cki_lib.yaml del foo.1 <<< 'foo: [bar, baz]'
---
foo:
  - bar
```

## Dump YAML data

```bash
usage: python3 -m cki_lib.yaml dump [-h] [--format {yaml,json}]

Dump YAML data

options:
  -h, --help  show this help message and exit
  --format {yaml,json}  output format (default: yaml)
```

Reads from stdin and writes to stdout.

Example:

```console
$ python -m cki_lib.yaml dump <<< 'foo: [bar, baz]'
---
foo:
  - bar
  - baz
```

## Validate YAML files with a JSON schema

```bash
usage: python3 -m cki_lib.yaml validate [-h]
                                        [--schema SCHEMA]
                                        [--process-config-tree]
                                        [--resolve-includes]
                                        [--resolve-references]
                                        [files ...]
Validation of YAML files

positional arguments:
  files            paths of YAML files to validate

options:
  -h, --help             show this help message and exit
  --schema SCHEMA        path of JSON schema file
  --process-config-tree  support .default and .extends
  --resolve-includes     support .include
  --resolve-references   support !reference tags
```

Reads from stdin if no files are passed.

### CKI schema support

The validation code tries to determine the values for the following parameters automatically:

| parameter               | where       | key                                                |
|-------------------------|-------------|----------------------------------------------------|
| `--schema`              | YAML file   | `.schema` specified as `python.module/schema-file` |
| `--process-config-tree` | JSON schema | `$ckiProcessConfigTree`                            |
| `--resolve-includes`    | JSON schema | `$ckiResolveIncludes`                              |
| `--resolve-references`  | JSON schema | `$ckiResolveReferences`                            |

### Exit codes

| Code | Description                                         |
|------|-----------------------------------------------------|
| 0    | No validation error found                           |
| 1    | At least one file failed schema validation          |
| 2    | CLI invocation error, e.g. when no files are passed |
