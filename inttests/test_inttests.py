"""Tests for cki_lib.inttests."""
import os
from unittest import mock

from cki_lib import messagequeue
from cki_lib import session
from cki_lib import stomp
from cki_lib.inttests import cluster
from cki_lib.inttests.rabbitmq import RabbitMQServer

SESSION = session.get_session(__name__)


@cluster.skip_without_requirements()
class RabbitMQIntegrationTest(RabbitMQServer):
    """Tests for cki_lib.inttests.rabbitmq."""

    def test_external_auth(self) -> None:
        """Test message queue external auth."""
        messagequeue.MessageQueue().send_message({'foo': 'bar'}, 'queue-name')

    @mock.patch.dict('os.environ', {
        'RABBITMQ_PORT': '5672',
        'RABBITMQ_CAFILE': '',
        'RABBITMQ_CERTFILE': '',
    })
    def test_password_auth(self) -> None:
        """Test message queue password auth."""
        messagequeue.MessageQueue().send_message({'foo': 'bar'}, 'queue-name')

    def test_stomp_cert(self) -> None:
        """Test stomp client cert auth."""
        stomp.StompClient().send_message({'foo': 'bar'}, 'queue-name')

    def test_management_interface(self) -> None:
        """Test the management interface."""
        host = os.environ['RABBITMQ_HOST']
        management_auth = (os.environ['RABBITMQ_USER'], os.environ['RABBITMQ_PASSWORD'])

        cases = (
            ('https', int(os.environ['RABBITMQ_MANAGEMENT_PORT'])),
            ('http', 15672),
        )

        for protocol, port in cases:
            with self.subTest(protocol):
                SESSION.get(f'{protocol}://{host}:{port}/api/policies/%2F',
                            verify=os.environ['RABBITMQ_CAFILE'],
                            auth=management_auth,
                            ).raise_for_status()
