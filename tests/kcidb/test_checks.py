"""Test kcidb/validate.py classes and functions."""
import unittest
from unittest import mock

from cki_lib.kcidb import checks


class TestChecks(unittest.TestCase):
    """Test KCIDB checks."""

    def test_broken_boot_tests(self) -> None:
        """Test broken_boot_tests."""
        cases = (
            (
                "all error",
                [
                    mock.Mock(id=1, build_id=1, comment="Boot test", status="ERROR", waived=None),
                    mock.Mock(id=2, build_id=1, comment="Boot test", status="ERROR", waived=None),
                    mock.Mock(id=3, build_id=2, comment="Boot test", status="PASS", waived=None),
                    mock.Mock(id=4, build_id=2, comment="Boot test", status="PASS", waived=None),
                ],
                {1, 2},
            ),
            (
                "all error/miss",
                [
                    mock.Mock(id=1, build_id=1, comment="Boot test", status="ERROR", waived=None),
                    mock.Mock(id=2, build_id=1, comment="Boot test", status="MISS", waived=None),
                    mock.Mock(id=3, build_id=2, comment="Boot test", status="PASS", waived=None),
                    mock.Mock(id=4, build_id=2, comment="Boot test", status="PASS", waived=None),
                ],
                {1, 2},
            ),
            (
                "all miss should return empty because there was a provision failure",
                [
                    mock.Mock(id=1, build_id=1, comment="Other", status="MISS", waived=None),
                    mock.Mock(id=2, build_id=1, comment="Test", status="MISS", waived=None),
                    mock.Mock(id=3, build_id=2, comment="Boot test", status="MISS", waived=None),
                    mock.Mock(id=4, build_id=2, comment="Boot test", status="MISS", waived=None),
                ],
                set(),
            ),
            (
                "error waived, all the rest miss should also count as a provision failure",
                [
                    mock.Mock(id=1, build_id=1, comment="Provision", status="ERROR", waived=True),
                    mock.Mock(id=2, build_id=1, comment="Other", status="MISS", waived=None),
                    mock.Mock(id=3, build_id=1, comment="Test", status="MISS", waived=None),
                    mock.Mock(id=4, build_id=2, comment="Boot test", status="ERROR", waived=True),
                    mock.Mock(id=5, build_id=2, comment="Boot test", status="MISS", waived=None),
                    mock.Mock(id=6, build_id=2, comment="Boot test", status="MISS", waived=None),
                ],
                set(),
            ),
            (
                "one error, one fail should count as at least one boot",
                [
                    mock.Mock(id=1, build_id=1, comment="Boot test", status="ERROR", waived=None),
                    mock.Mock(id=2, build_id=1, comment="Boot test", status="FAIL", waived=None),
                    mock.Mock(id=3, build_id=2, comment="Boot test", status="PASS", waived=None),
                    mock.Mock(id=4, build_id=2, comment="Boot test", status="PASS", waived=None),
                ],
                set(),
            ),
            (
                "one error, one pass should count as at least on boot",
                [
                    mock.Mock(id=1, build_id=1, comment="Boot test", status="ERROR", waived=None),
                    mock.Mock(id=2, build_id=1, comment="Boot test", status="PASS", waived=None),
                    mock.Mock(id=3, build_id=2, comment="Boot test", status="PASS", waived=None),
                    mock.Mock(id=4, build_id=2, comment="Boot test", status="PASS", waived=None),
                ],
                set(),
            ),
            (
                "no boot test",
                [
                    mock.Mock(id=1, build_id=1, comment="Other", status="ERROR", waived=None),
                    mock.Mock(id=2, build_id=1, comment="Test", status="ERROR", waived=None),
                    mock.Mock(id=3, build_id=2, comment="Boot test", status="PASS", waived=None),
                    mock.Mock(id=4, build_id=2, comment="Boot test", status="PASS", waived=None),
                ],
                set(),
            ),
            (
                "multiple",
                [
                    mock.Mock(id=1, build_id=1, comment="Boot test", status="ERROR", waived=None),
                    mock.Mock(id=2, build_id=1, comment="Boot test", status="MISS", waived=None),
                    mock.Mock(id=3, build_id=2, comment="Boot test", status="ERROR", waived=None),
                    mock.Mock(id=4, build_id=2, comment="Boot test", status="MISS", waived=None),
                ],
                {1, 2, 3, 4},
            ),
        )

        for description, data, expected_test_ids in cases:
            with self.subTest(description):
                self.assertEqual({t.id for t in checks.broken_boot_tests(data)}, expected_test_ids)

    def test_triaged_boot_tests(self) -> None:
        """Test broken_boot_tests with waiving."""
        cases = (
            ("no dw_all", None, {1}),
            (
                "test triaged",
                mock.Mock(
                    testresults=[],
                    issueoccurrences=[mock.Mock(test_id=1, is_regression=False)],
                ),
                set(),
            ),
            (
                "test regression",
                mock.Mock(
                    testresults=[],
                    issueoccurrences=[mock.Mock(test_id=1, is_regression=True)],
                ),
                {1},
            ),
            (
                "testresult triaged",
                mock.Mock(
                    testresults=[mock.Mock(id=2, test_id=1)],
                    issueoccurrences=[mock.Mock(testresult_id=2, is_regression=False)],
                ),
                set(),
            ),
            (
                "testresult regression",
                mock.Mock(
                    testresults=[mock.Mock(id=2, test_id=1)],
                    issueoccurrences=[mock.Mock(testresult_id=2, is_regression=True)],
                ),
                {1},
            ),
        )

        tests = [mock.Mock(id=1, build_id=1, comment="Boot test", status="ERROR", waived=None)]

        for description, dw_all, expected_test_ids in cases:
            with self.subTest(description):
                result = checks.broken_boot_tests(tests, dw_all=dw_all)
                self.assertEqual({t.id for t in result}, expected_test_ids)

    def test_no_tests(self) -> None:
        """Test broken_boot_tests without an explicit test list."""
        dw_all = mock.Mock(
            tests=[
                mock.Mock(id=1, build_id=1, comment="Boot test", status="ERROR", waived=None),
                mock.Mock(id=2, build_id=2, comment="Boot test", status="ERROR", waived=None),
            ],
            testresults=[],
            issueoccurrences=[
                mock.Mock(test_id=1, is_regression=False),
                mock.Mock(test_id=2, is_regression=True),
            ],
        )

        self.assertEqual({t.id for t in checks.broken_boot_tests(dw_all=dw_all)}, {2})
